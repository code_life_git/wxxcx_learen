Page({

  data: {
     array: [1,2,3,4,5,6,7,8,9,10,11,12],
    arrayObj:[
        {id:1001,name: "jack" },
        {id:1002,name: "lily" },
        {id:1003,name: "jay" },
        {id:1004,name: "mike" }
    ],
    showme: "请选择一个人名",
    arrayMul: [
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    ],
    arrayObjMul: [
      [
        { id: 1001, name: "jack" },
        { id: 1002, name: "lily" },
        { id: 1003, name: "jay" },
        { id: 1004, name: "mike" }
      ],
      [
        { id: 1001, name: "jack" },
        { id: 1002, name: "lily" },
        { id: 1003, name: "jay" },
        { id: 1004, name: "mike" }
      ]
    ]
  },
  changeFun: function(e){
     var index= e.detail.value;
     console.log("index的下标为:" + index);

     var id=this.data.arrayObj[index].id;
     var name =this.data.arrayObj[index].name;
     
     this.setData({
       showme:  name 
     })

  },
  cancelFun: function(e){
     console.log("触发取消事件");
  },
  columnchange : function(e){
    console.log(e.detail)
  },
  columnchangeMulti : function(e){
    var indexs= e.detail.value;
    var arrayObjMulti = this.data.arrayObjMul;
    
    for (var i=0;i<indexs.length;i++){
      var indexTemp=indexs[i];
      
      var id=arrayObjMulti[i][indexTemp].id;
      var name = arrayObjMulti[i][indexTemp].name;
      console.log(id+"  "+name);
    }
  }
})