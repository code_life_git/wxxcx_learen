Page({
  data: {
      color: "white"
  },
  changeme: function(e){
    var flag = e.detail.value;
    
    //flag 为true 代表为选中的状态
    if(flag){
      this.setData({
        color: "white"
      })

    } else{
      this.setData({
        color: "black"
      })
    }
  }

})