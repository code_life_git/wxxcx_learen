// common.js
function sayHello(name) {
  console.log(`Hello ${name} !`)
}
function sayGoodbye(name) {
  console.log(`Goodbye ${name} !`)
}

// 暴露接口
module.exports.sayHello = sayHello
exports.sayGoodbye = sayGoodbye