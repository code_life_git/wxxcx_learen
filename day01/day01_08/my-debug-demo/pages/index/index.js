//index.js
//获取应用实例
// Page对象是一定要有的
Page({
  // data为数据绑定
  data: {
    mytext: '第一个demo',
    color: "red"
  },
  onLoad: function () {
    // getApp 为获取小程序的实例
    var appInstance = getApp();
    // appInstance.courseName 为获取自定义的参数
    console.log(appInstance.courseName);
    this.setData({
      mytext: appInstance.courseName
    })
  }
})
