//初始化的数据 并用\n换行
const initData = 'this is first line\nthis is second line'
//定义一个数组,用于添加或者删除行 
const extraLine = []
Page({
  data: {
    text: initData //把初始化的数据绑定到页面中
  },
  // 点击添加按钮触发的时间
  add(e) {
    // 往数组中添加行 
    extraLine.push('other line')
    this.setData({
      // 如果是增加一行, 那么加上初始化的数据加上一行
      //并且把数组中所有的数据拼接成一个字符串, 用\n 隔开用于换行
      text: initData + '\n' + extraLine.join('\n')
    })
  },
  remove(e) {
    if (extraLine.length > 0) {
      //pop() 方法用于删除数组的最后一个元素。
      extraLine.pop()
      this.setData({
        text: initData + '\n' + extraLine.join('\n')
      })
    }
  }
})